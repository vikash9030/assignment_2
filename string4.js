// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

function string4(data){
    var full_name = "";
    if(data.first_name !== undefined){
        full_name+=data.first_name+" ";
    }
    if(data.middle_name !== undefined){
        full_name+=data.middle_name+" ";
    }
    if(data.last_name !== undefined){
        full_name+=data.last_name;
    }
    return full_name;
}
// var first_name="";
// var last_name="";
// var middle_name="";
// var data = {"first_name": "JoHN",  "last_name": "SMith"};
// let result = string4(data);
// console.log(result);
module.exports = string4;
