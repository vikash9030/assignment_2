function string3(date){
   var month="";
   for(var i=0;i<date.length;i++){
       if(date.charAt(i)==='/'){
           break;
       }else{
           month+=date.charAt(i);
       }
   }
   //console.log(month);
   var month_name ="";
   switch(month){
       case "1":
           month_name = "January";
           break;
       case "2":
           month_name = "February";
           break;       
       case "3":
           month_name = "March";
           break;
       case "4":
           month_name = "April";
           break;
       case "5":
           month_name = "May";
           break;
       case "6":
           month_name = "June";
           break;
       case "7":
           month_name = "July";
           break;
       case "8":
           month_name = "August";
           break;
       case "9":
           month_name = "September";
           break;
       case "10":
           month_name = "October";
           break;
       case "11":
           month_name = "November";
           break;
       case "12":
           month_name = "December";
           break;

   }
 return month_name;
       
}
// var date = "1/2/2002";
// let result = string3(date);
// console.log(result);
module.exports = string3;
